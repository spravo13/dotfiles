alias g="grep"
alias ls="ls --color=auto"

alias sb='source ~/.bashrc'
alias r='reset'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'

alias mv='mv -i'
alias cp='cp -i'
alias ln='ln -i'
alias rm='rm -i'

alias vnc='vncviewer'
alias vncv='vncviewer'
alias vncs='/usr/lib/vino/vino-server'
alias vncserver='/usr/lib/vino/vino-server'
alias ftpserver='sudo service vsftpd start'
alias ftpserver-start='sudo service vsftpd start'
alias ftpserver-stop='sudo service vsftpd stop'
alias irc='irssi'

alias p='cd ~/projects'
alias d='cd ~/dotfiles'
